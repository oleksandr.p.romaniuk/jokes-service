package com.example.jokesservice.service;

import com.example.jokesservice.model.Joke;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface JokeService {
    CompletableFuture<List<Joke>> getJokes(int count);
}