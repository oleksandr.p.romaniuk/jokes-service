package com.example.jokesservice.service;

import com.example.jokesservice.model.Joke;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Async
@Service
public class OfficialJokeService implements JokeService {
    private final String JOKE_API_URL = "https://official-joke-api.appspot.com/random_joke";
    private static final int MAX_JOKE_BATCH_SIZE = 10;
    private final RestTemplate restTemplate;

    @Autowired
    public OfficialJokeService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public CompletableFuture<List<Joke>> getJokes(int count) {
        int batches = count / MAX_JOKE_BATCH_SIZE + (count % MAX_JOKE_BATCH_SIZE == 0 ? 0 : 1);
        List<CompletableFuture<List<Joke>>> futures = new ArrayList<>();

        for (int i = 0; i < batches; i++) {
            futures.add(fetchJokes(Math.min(MAX_JOKE_BATCH_SIZE, count)));
            count -= MAX_JOKE_BATCH_SIZE;
        }

        return CompletableFuture.allOf(futures.toArray(new CompletableFuture[0]))
                .thenApply(v -> futures.stream()
                        .map(CompletableFuture::join)
                        .flatMap(List::stream)
                        .collect(Collectors.toList()));
    }

    private CompletableFuture<List<Joke>> fetchJokes(int count) {
        return CompletableFuture.supplyAsync(() -> {
            List<Joke> jokes = new ArrayList<>();
            for (int i = 0; i < count; i++) {
                try {
                    jokes.add(restTemplate.getForObject(JOKE_API_URL, Joke.class));
                } catch (HttpClientErrorException.TooManyRequests e) {
                    // Log the exception and move on to the next joke
                    Logger logger = LoggerFactory.getLogger(JokeService.class);
                    logger.error("Too many requests to the joke API. Try again later.", e);
                }
            }
            return jokes;
        });
    }
}
