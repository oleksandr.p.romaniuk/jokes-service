package com.example.jokesservice.controller;

import com.example.jokesservice.model.Joke;
import com.example.jokesservice.service.JokeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
public class JokeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(JokeController.class);
    private final JokeService jokeService;

    public JokeController(JokeService jokeService) {
        this.jokeService = jokeService;
    }

    @GetMapping("/jokes")
    public CompletableFuture<ResponseEntity<List<Joke>>> getJokes(@RequestParam(defaultValue = "5") int count) {
        if (count > 100) {
            LOGGER.error("Attempt to request {} jokes, but limit is 100", count);
            throw new IllegalArgumentException("You can receive no more than 100 jokes at a time.");
        }
        LOGGER.info("Request for {} jokes received", count);
        // use JokeService to fetch the jokes
        return jokeService.getJokes(count)
                .thenApply(ResponseEntity::ok)
                .exceptionally(ex -> ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build());
    }
}
