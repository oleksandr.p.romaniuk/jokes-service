package com.example.jokesservice.controller;

import com.example.jokesservice.model.Joke;
import com.example.jokesservice.service.JokeService;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class JokeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JokeService jokeService;

    @Test
    public void getJokes_ValidRequest_ReturnsStatusOk() throws Exception {
        List<Joke> jokes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Joke joke = new Joke();
            joke.setSetup("Joke " + (i+1));
            joke.setPunchline("Punchline " + (i+1));
            jokes.add(joke);
        }

        when(jokeService.getJokes(anyInt())).thenReturn(CompletableFuture.completedFuture(jokes));

        mockMvc.perform(get("/jokes")
                        .param("count", "5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void getJokes_InvalidRequest_ReturnsStatusBadRequest() throws Exception {
        mockMvc.perform(get("/jokes")
                        .param("count", "101")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void getJokes_ValidRequest_CorrectNumberOfJokes() throws Exception {
        List<Joke> jokes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Joke joke = new Joke();
            joke.setSetup("Joke " + (i+1));
            joke.setPunchline("Punchline " + (i+1));
            jokes.add(joke);
        }

        when(jokeService.getJokes(anyInt())).thenReturn(CompletableFuture.completedFuture(jokes));

        MvcResult mvcResult = mockMvc.perform(get("/jokes")
                        .param("count", "5")
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        mvcResult = mockMvc.perform(asyncDispatch(mvcResult))
                .andExpect(status().isOk())
                .andReturn();

        String responseBody = mvcResult.getResponse().getContentAsString();

        System.out.println("Response body: " + responseBody);

        assertTrue(responseBody.contains("\"setup\":\"Joke 1\""), "Response does not contain first joke setup");
        assertTrue(responseBody.contains("\"setup\":\"Joke 5\""), "Response does not contain fifth joke setup");
        assertTrue(responseBody.split("\"setup\":").length - 1 == 5, "Response does not contain correct number of jokes");
    }

}
